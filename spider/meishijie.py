import re
import requests
from bs4 import BeautifulSoup
import json
import os
import time


class spiderMeiShiJjie():
    def __init__(self):
        self.main_url = 'https://www.meishij.net/shicai/'
        self.headers = {
            'Host': 'www.meishij.net',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}
        self.save_path = "../data/美食杰/"

    def get_single(self, url):
        headers = self.headers
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        temp_result = []
        temp_name = []
        result_dict = {}
        for element in soup.find_all(class_='main'):
            for element_one in element.find_all(class_='sc_header_con1'):
                for element_two in element_one.find_all('h1'):
                    result_dict['食材名字'] = element_two.get_text()
                for element_three in element_one.find_all(class_='p2'):
                    text = element_three.get_text()
                    text = re.sub("[\t\r \xa0]", '', text)
                    title_key = re.findall("【(.*?)】", text)
                    title_value = [i for i in re.sub("【(.*?)】", '', text).split('：') if i != '']
                    for nn in range(len(title_key)):
                        result_dict[title_key[nn]] = title_value[nn]
            for element_four in element.find_all(class_='sccon_right_w'):
                for element_five in element_four.find_all(class_='sccon_right_con'):
                    for element_six in element_five.find_all(['p', 'strong', 'li']):
                        temp_result.append(
                            [i for i in re.sub("[\t\r \xa0]", '', element_six.get_text()).split("\n") if i != ''])
                        temp_name.append(element_six.name)
        count = []
        for i in range(len(temp_result) - 1):
            if temp_result[i] == temp_result[i + 1]:
                count.append(i + 1)
            elif len(temp_result[i + 1]) > 0:
                if temp_result[i + 1][0] in temp_result[i]:
                    count.append(i + 1)

        temp_ = [temp_result[i] for i in range(len(temp_result)) if i not in count]
        temp_name_ = [temp_name[i] for i in range(len(temp_name)) if i not in count]
        key_index = [i for i in range(len(temp_)) if temp_name_[i] == 'strong']
        key_name = [temp_[i][0] for i in range(len(temp_)) if temp_name_[i] == 'strong']
        data_dict = dict(zip(key_name, [[] for m in range(len(key_name))]))
        key_range = [[key_index[i], key_index[i + 1]] for i in range(len(key_index) - 1)]
        key_range.append([key_index[-1], len(temp_name_)])
        for i in range(len(temp_)):
            for j in range(len(key_range)):
                if i > key_range[j][0] and i < key_range[j][1]:
                    data_dict[key_name[j]].append(temp_[i])
        result_dict.update(data_dict)
        print(json.dumps(result_dict, ensure_ascii=False, indent=2))

    def get_food(self, url):
        baike_url = []
        food_url = []
        for i in range(1, 1000):
            url = url + '/p' + str(i) + '/'
            response = requests.get(url, headers=self.headers)
            soup = BeautifulSoup(response.text, 'html.parser')
            page_text = []
            for element_ in soup.find_all(class_='listtyle1_page_w'):
                page_text.append(element_.get_text())
            if i == 1:
                for chidren in soup.find_all(class_='pathstlye1'):
                    for chidren_one in chidren.find_all('li'):
                        for chidren_two in chidren_one.find_all('a'):
                            baike_url.append(chidren_two['href'])
            if '下一页' in ''.join(page_text):
                for element in soup.find_all(class_='cpjhw'):
                    for element_one in element.find_all(class_='listtyle1'):
                        for element_two in element_one.find_all('a'):
                            food_url.append(element_two['href'])
            else:
                break
        return food_url, baike_url[2]

    def get_shicaibaike(self):
        response = requests.get(self.main_url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        result = {}
        for element in soup.find_all(class_='listnav_dl_style1 w990 bb1 clearfix'):
            key_ = ''
            for element_one in element.find_all('dt'):
                key_ = element_one.get_text()
            value = []
            for element_two in element.find_all('dd'):
                for element_three in element_two.find_all('a'):
                    temp = {}
                    temp[element_three.get_text()] = element_three['href']
                    value.append(temp)
            result[key_] = value
        with open(self.save_path + 'meishijie.json', "w", encoding="utf8") as dump_f:
            json.dump(result, dump_f, ensure_ascii=False, indent=2)
        return result


if __name__ == '__main__':
    spiderMeiShiJjie().get_single('https://www.meishij.net/%E7%99%BD%E8%8F%9C')
