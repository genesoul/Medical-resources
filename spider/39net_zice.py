import re
import requests
from bs4 import BeautifulSoup
import json
import os
import pymongo


class spiderCheck39():
    """
    39net检查数据爬取
    """

    def __init__(self):
        self.headers = {'Host': 'test.39.net',
                        'Upgrade-Insecure-Requests': '1',
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                        'Accept-Encoding': 'gzip, deflate, br',
                        'Connection': 'keep-alive',
                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}

    def get_single(self, url):
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        result = {}
        page = ''.join([element.get_text() for element_one in soup.find_all(id='test_box') for element in
                        element_one.find_all("b")])
        note = []
        result['问题数量'] = page
        label = []
        content = []
        result_list = []
        for element in soup.find_all(id='formContent'):
            for element_one in element.find_all(id='divDesc'):
                note.append(element_one.get_text())
            for element_two in element.find_all(class_='problem'):
                for element_three in element_two.find_all('p'):
                    for element_four in element_three.find_all('strong'):
                        content.append(element_four.get_text())
                for element_five in element_two.find_all('div'):
                    for element_six in element_five.find_all("label"):
                        temp = {}
                        temp['问题'] = re.sub("[\t\r \xa0]", '', element_six.get_text())
                        temp['问题跳转'] = element_six['for']
                        label.append(temp)
            if 'complete' in str(element):
                for element_seven in element.find_all(class_='problem'):
                    result_list.append(element_seven.get_text())
        result['摘要'] = re.sub("[\n\t\r \xa0]", '', ''.join(note))
        result['答案'] = label
        result['问题'] = re.sub("[\n\t\r \xa0]", '', ''.join(content))
        result['url'] = url
        if result_list != []:
            result['结果'] = re.sub("[\n\t\r \xa0]", '', ''.join(result_list))
        return result

    def get_order(self, url):
        content = []
        data = [self.get_single(url)]
        number = int(data[0]['问题数量']) + 3
        content.append(data)
        count = 1
        status = [data[0]['url']]
        while count <= number:
            temp_all = []
            for element_one in data:
                for element in element_one['答案']:
                    temp_url = url.replace('.html', '') + '-' + str(count) + '-' + element['问题跳转'].replace('Lab_',
                                                                                                 '') + '.html'
                    temp_result = self.get_single(temp_url)
                    print(temp_url)
                    if temp_result['url'] not in status:
                        status.append(temp_result['url'])
                        content.append(temp_result)
                    temp_all.append(temp_result)
            count = count + 1
            data = temp_all
            print(count,len(data))
        for element in content:
            print(element)


print(spiderCheck39().get_order('http://test.39.net/test/4562.html'))
