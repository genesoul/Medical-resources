import re
import requests
from bs4 import BeautifulSoup
import json
import os
import time
import uuid


class spiderTiJian():
    """
    爬取上海体检套餐
    """
    def __init__(self):
        self.headers = {
            'Host': 'www.viptijian.com',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}
        self.save_path = "../data/上海体检套餐/"

    def get_all_url(self):
        """
        获取全部体检链接
        :return: 
        """
        result = []
        for i in range(1, 129):
            response = requests.get("https://www.viptijian.com/021/cl?p=" + str(i), headers=self.headers)
            soup = BeautifulSoup(response.text, 'html.parser')
            for element_one in soup.find_all(class_='set_list_total'):
                for children in element_one.find_all(class_='set_list'):
                    for element_two in children.find_all(class_='set_list_content'):
                        for element_three in element_two.find_all('a'):
                            print(element_three['href'])
                            result.append(element_three['href'])
        with open(self.save_path + 'tijian.json', "w", encoding="utf8") as dump_f:
            json.dump({"url": result}, dump_f, ensure_ascii=False, indent=2)
        return result

    def get_single(self, url):
        """
        爬取单页体检信息
        :param url: 
        :return: 
        """
        content_dict = {}
        content_dict['url'] = url
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        for element in soup.find_all(class_='merchandise'):
            for element_one in element.find_all(class_='commodity_right'):
                for element_two in element_one.find_all('h1'):
                    content_dict["name"] = element_two.get_text()
                for element_three in element_one.find_all('p'):
                    if 'commodity_right_one' in str(element_three):
                        content_dict["detail"] = element_three.get_text()
                for element_four in element_one.find_all(class_='commodity_right_two'):
                    for element_five in element_four.find_all(class_='right_two_a_a'):
                        content_dict["price"] = re.sub("[\n\t\r \xa0]", '', str(element_five.get_text()))
        for element in soup.find_all(class_='set_details_one'):
            for i, element_one in enumerate(element.find_all('p')):
                temp = re.sub("[\n\t\r \xa0]", '', str(element_one.get_text())).split(":")
                if len(temp) > 1:
                    content_dict[temp[0]] = temp[1]
                elif len(temp) == 0:
                    content_dict[str(i)] = temp[0]
        data_all = []
        for element in soup.find_all(class_='examine_details_white'):
            key = ''
            for element_two in element.find_all('h2'):
                key = str(element_two.get_text())
            for element_three in element.find_all(class_='tc_cont_box'):
                value = {}
                temp_value = []
                for element_four in element_three.find_all('h3'):
                    key_ = re.sub("[\n\t\r \xa0]", '', str(element_four.get_text()))
                for element_five in element_three.find_all(class_='Programlistbox'):
                    temp_value_ = {}
                    for element_six in element_five.find_all(class_='Programname-txt'):
                        temp_key_ = re.sub("[\n\t\r \xa0]", '', str(element_six.get_text()))
                    for element_seven in element_five.find_all(class_='Programworth-txt'):
                        temp_value_[temp_key_] = re.sub("[\n\t\r \xa0]", '', str(element_seven.get_text()))
                        temp_value.append(temp_value_)
                value[key_] = temp_value
                data_all.append(value)
        content_dict['检查'] = data_all
        yuyue = []
        for element in soup.find_all(class_='details_one_row clearfix'):
            for element_one in element.find_all('p'):
                temp1_dict = {}
                temp1 = re.sub("[\n\t\r \xa0]", '', str(element_one.get_text()))
                key_temp1 = temp1[0:temp1.find(':')]
                temp1_dict[key_temp1] = temp1[temp1.find(':') + 1:]
                yuyue.append(temp1_dict)
        tijian_data = []
        for element in soup.find_all(class_='examine_appointment_look_list'):
            key1 = []
            for element_one in element.find_all(class_='examine_appointment_look_list_left'):
                for element_two in element_one.find_all('div'):
                    key1.append(re.sub("[\n\t\r \xa0]", '', str(element_two.get_text())))
            attention_all = []
            for element_three in element.find_all(class_='examine_appointment_look_list_right clearfix'):
                att_temp = []
                for element_four in element_three.find_all('li'):
                    att_temp.append(re.sub("[\n\t\r \xa0]", '', str(element_four.get_text())))
                attention_all.append(att_temp)
                # print("---------------------")
            tijian_data.append(dict(zip(key1, attention_all)))
        content_dict['体检'] = tijian_data
        content_dict['预检'] = yuyue
        return content_dict

    def get_all(self):
        """
        爬取全部体检信息
        :return: 
        """
        with open(self.save_path + 'tijian.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)
        all_url = list(set(data_json['url']))
        for element in all_url:
            if element.find('combo') != -1:
                try:
                    result = self.get_single("https://www.viptijian.com" + element)
                    name = str(uuid.uuid1())
                    with open(self.save_path + name + '.json', "w", encoding="utf8") as dump_f:
                        json.dump(result, dump_f, ensure_ascii=False, indent=2)
                    print(element)
                except:
                    print(element + 'fail')
                time.sleep(0.1)

    def single_handle(self, path):
        """
        单个体检套餐数据清洗
        :param path: 
        :return: 
        """
        result = []
        with open(self.save_path + path, encoding='utf8') as data_json:
            data_json = json.load(data_json)['url']
        temp_name = re.findall(r'【(.+?)】', data_json['name'])
        if len(temp_name) > 0:
            tijianleibie = jibie = temp_name[0]
        else:
            tijianleibie = jibie = ''
        biaoti = re.sub(r'【(.+?)】', '', data_json['name'])
        if '适用人群' in data_json:
            renqun = data_json['适用人群']
        else:
            renqun = ''
        if '体检机构' in data_json:
            dengji_ = re.findall(r'【(.+?)】', data_json['体检机构'])
            pingpai = re.sub(r'【(.+?)】', '', data_json['体检机构'])
            if len(dengji_) > 0:
                dengji = dengji_[0]
            else:
                dengji = ''
        else:
            dengji = ''
            pingpai = ''
        if '适用性别' in data_json:
            xinbie = data_json['适用性别']
        else:
            xinbie = ''
        jiage_ = re.findall("\d+", data_json['price'])
        if len(jiage_) > 0:
            jiage = jiage_[0]
        else:
            jiage = ''
        beizhu = data_json['detail']
        for element in data_json['检查']:
            for key, value in element.items():
                xmlb = key
                for element_one in value:
                    for key, value in element_one.items():
                        tijianxiangmu = key
                        tijianneirong = value
                        result.append((biaoti, tijianleibie, renqun, jibie, dengji, pingpai, xinbie, jiage, beizhu,
                                       xmlb, tijianxiangmu, tijianneirong))
        return result
    def all_handle(self):
        """
        全部体检套餐数据清洗
        :return: 
        """
        for file in os.listdir(self.save_path):
                data = self.single_handle(file)


if __name__ == '__main__':
    print(spiderTiJian().get_all())
