import re
import requests
from bs4 import BeautifulSoup
import json


class xinXueGuan():
    def __init__(self):
        self.main_url = ['http://www.heartabc.com/xxgbdq/xxgnkcjjb/#1', 'http://www.heartabc.com/xxgbdq/xxgnkqtjb/',
                         'http://www.heartabc.com/xxgbdq/xxwkqtjb/', 'http://www.heartabc.com/xxgbdq/xxwkcjjb/']
        self.headers = {
            'Host': 'www.heartabc.com',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}
        self.save_path = "../data/华夏心血管/"

    def get_single(self, url):
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        text = [element_one.get_text() for element in soup.find_all(class_='news-content') for element_one in
                element.find_all('p')]
        text = [i for i in text if i != '\xa0']
        result = {}
        for i in range(len(text) - 1):
            temp_content = re.split('[:：]', text[i])
            if len(temp_content) > 1:
                key_ = temp_content[0]
                value = temp_content[1]
                value = [i for i in re.split('[\xa0, ]', value) if i != '']
            else:
                key_ = str(i)
                value = temp_content
            result[key_] = value
        result.update({"简介": text[-1]})
        return result

    def get_url(self):
        content = []
        for url in self.main_url:
            response = requests.get(url, headers=self.headers)
            soup = BeautifulSoup(response.text, 'html.parser')
            for element in soup.find_all(class_='cb xxg-list'):
                for element_one in element.find_all('ul'):
                    for element_two in element_one.find_all('li'):
                        for element_three in element_two.find_all('a'):
                            content.append(self.get_single("http://www.heartabc.com" + element_three['href']))
        with open(self.save_path + 'data.json', "w", encoding="utf8") as dump_f:
            json.dump({"result": content}, dump_f, ensure_ascii=False, indent=2)

    def handle(self):
        content =[]
        res =[]
        with open(self.save_path + 'data3.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)['result']
            for element in data_json:
                print([key for key, value in element.items()], element['疾病名称'])






# 风心病 扩张型心肌病
xinXueGuan().handle()
