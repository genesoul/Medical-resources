幽门螺杆菌感染	
湿疹	eczema
腰椎间盘突出症	lumbar disc herniation	LDH
糖尿病	diabetes mellitus	DM
孤独症	autism
带状疱疹	herpes zoster
川崎病	Kawasaki disease	KD
痤疮	acne
前列腺炎	prostatitis
处女膜闭锁	imperforate hymen
痛风	gout
多囊卵巢综合征	polycystic ovary syndrome	PCOS
肾虚	
股癣	tinea cruris
梅毒	syphilis
过敏性鼻炎	allergic rhinitis
脑梗死	cerebral infarction
口腔溃疡	canker sore	oral ulcer
艾滋病	acquired immunodeficiency syndrome	AIDS
足癣	tinea pedis
肾结石	renal calculus
红斑狼疮	lupus erythematosus	LE
尿毒症	uremia
脂溢性皮炎	seborrheic dermatitis
高度近视	high myopia
乳腺增生症	breast hyperplasia
强直性脊柱炎	ankylosing spondylitis	AS
急性阑尾炎	acute appendicitis
中风	Stroke
异位妊娠	ectopic pregnancy
梦魇	nightmare
急性荨麻疹	
鼻咽癌	carcinoma of nasopharynx
白化病	albinism
白内障	cataract
肝火旺	
胃溃疡	gastric ulcer	GU
肾阴虚	
脂溢性脱发	androgenetic alopecia	AGA
脑出血	intracerebral hemorrhage	ICH
鼻炎	rhinitis
神经官能症	neurosis
假性近视	pseudomyopia
宫颈炎	cervicitis
肺癌	lung cancer
阵发性室上性心动过速	paroxysmal supraventricular tachycardia	PSVT
腓骨肌萎缩症	peronial myoatrophy
阴挺	
无色素痣	achromic nevus
颈椎病	cervical spondylosis
垂体卒中	pituitary apoplexy	PA
稽留流产	missed abortion
肿瘤	tumor
鼻窦炎	sinusitis
阴虚	
乳腺癌	breast cancer
过敏性紫癜	allergic purpura
阳痿	impotence
胰腺癌	pancreatic carcinoma/cancer
铜绿假单胞菌感染	Pseudomonas aeruginosa infection
先兆流产	threatened abortion
痔	haemorrhoids
玫瑰糠疹	pityriasis rosea
肾功能不全	renal insufficiency
飞蚊症	muscae volitantes
口腔癌	oral cavity carcinomas
尿路感染	urinary tract infection	UTI
细菌性结膜炎	bacterial conjunctivitis
甲状腺结节	thyroid nodules	TNS
弱视	amblyopia
肝豆状核变性	hepatolenticular degeneration	HLD
包皮龟头炎	balanoposthitis
肺不张	atelectasis
卵巢颗粒细胞瘤	ovarian granulosa cell tumors	GCT
慢性咽炎	chronic pharyngitis
脑卒中	stroke
肺大疱	pulmonary bulla
角膜炎	keratitis
软骨瘤	chondroma
双角子宫	uterus bicornis
腰肌劳损	lumbar muscules strain
脾胃虚弱	
高原脑水肿	high altitude cerebral edema	HACE
多发性硬化症	multiple sclerosis	MS
子宫内膜异位症	endometriosis	EMT
系统性红斑狼疮	systemic lupus erythematosus
包茎	phimosis
输血反应	transfusion reaction
癣菌疹	dermatophytid
肌张力障碍	dystonia
类风湿关节炎	rheumatoid arthritis	RA
宫寒	
气血不足	
斑秃	alopecia areata
维生素C缺乏症	vitamin C deficiency
重症急性呼吸综合征	severe acute respiratory syndrome	SARS
胃癌	gastric carcinoma
男性尿道癌	Male urethra cancer
颈型颈椎病	cervical type cervicals pondylopathy
胃炎	gastritis
睑外翻	ectropion
双相障碍	bipolar disorder
坐骨神经痛	sciatica
盆腔炎	pelvic inflammatory disease	PID
酒渣鼻	
血管性水肿	angioedema
子宫癌	
旋毛虫病	trichinosis
粉刺	comedo
增生性贫血	hyperplastic anemia
肾炎	nephritis
基底细胞癌	basal cell carcinoma
贝赫切特综合征	Behcet syndrome
隆鼻后遗症	Rhinoplasty Syndrome
共同性斜视	comitant strabismus
胎儿宫内窘迫	
瘢痕体质	
坏血病	scurvy
脾脓肿	splenic abscess	SA
癌症	cancer	carcinoma
睡眠障碍	somnipathy
萎缩性胃炎	atrophic gastritis
夹腿综合征	Masturbation
气虚	
晕动病	motion sickness
骨坏死	osteonecrosis
秋季腹泻	autumn diarrhea
痱子	prickly heat	heat rash
性厌恶	sexual aversion
2型糖尿病	type 2 diabetes mellitus	T2DM
慢性荨麻疹	chronic urticaria
夜盲症	night blindness
心脏黏液瘤	cardiac myxoma
心脾两虚	
系统性血管炎	systemic vasculitis
阻塞性睡眠呼吸暂停低通气综合征	obstructive sleep apnea hypopnea syndrome	OSAHS
器质性心脏病	organic heart disease
下肢动脉硬化闭塞症	
脊髓血管病	vascular diseases of the spinal cord
肌病	myopathy
食管黏膜损伤	esophageal mucosa injury
多睾症	polyorchism
咽炎	pharyngitis
桥本甲状腺炎	hashimoto thyroiditis	HT
新生儿呼吸窘迫综合征	respiratory distress syndrome of newborn
前列腺肉瘤	prostate sarcoma	PS
牙外伤	dental trauma
阿米巴肝脓肿	amebic liver abscess
滴虫性阴道炎	trichomonal vaginitis	TV
良性阵发性位置性眩晕	benign　paroxysmal positional vertigo	BPPV
慢性心包炎	chronic pericarditis
子宫内膜不规则脱落	irregular shedding of endometrium
心脏神经症	cardiac neurosis
慢性嗜酸性粒细胞性肺炎	chronic eosinophilic pneumonia
牙龈癌	carcinoma of the gingiva
股骨头坏死	osteonecrosis of the femoral head	ONFH
痰饮	
先天性曲细精管发育不全综合征	Klinefelter's symdrome
巨人症	gigantism
急性胆源性胰腺炎	acute biliary pancreatitis	ABP
地方性斑疹伤寒	endemic typhus
雷诺综合征	Raynaud syndrome
干眼症	dry eye syndrome
慢性宫颈炎	chronic cervicitis
肠癌	intestinal cancer
牙周病	periodontal disease
妊娠期糖尿病	gestational diabetes mellitus	GDM
椎管内神经鞘瘤	intraspinal schwannoma
眼底黄斑水肿	macular edema
维生素B1缺乏症	
湿热	
肾上腺腺瘤	adrenal adenoma
隆突性皮肤纤维肉瘤	dermatofibrosarcoma protuberans	DFSP
智齿冠周炎	pericoronitis of wisdom tooth	PWT
冻疮	pernio
青盲	
颞下颌关节紊乱综合征	temporomandibular joint disturbance syndrome	TMJDS
过敏性结膜炎	allergic conjunctivitis
膝关节半月板损伤	meniscus injury of knee
鹅掌风	
新生儿湿肺	wet lung newborn
戈谢病	gaucher
牙周炎	periodontitis
胸廓出口综合征	thoracic outlet syndrome	 TOS
威廉姆斯综合征	Williams syndrome	WS
食管癌	carcinoma of esophagus
甲状腺功能亢进症	hyperthyroidism
干燥综合征	Sjögren syndrome	 SS
沙眼	trachoma
慢性胃炎	chronic gastritis
颈椎间盘突出症	cervical disc herniation
白喉	diphtheria
荨麻疹和血管性水肿	
胎盘早剥	placental abruption
黄褐斑	melasma
前置胎盘	placenta previa
呼吸衰竭	respiratory failure
胎儿乙醇综合征	fetal alcohol syndrome	FAS
慢性阻塞性肺疾病	chronic obstructive pulmonary disease	COPD
前庭大腺囊肿	bartholin cyst
鼻甲肥大	nasorostral hypertrophy
先天性耳前瘘管	congenital preauricular fistula
弥漫性胸膜间皮瘤	malignant pleural mesothelioma	MPM
舌骨骨折	hyoid bone fracture	HBF
乳腺良性肿瘤	
烧伤感染	infection of burn
产褥中暑	puerperal heat stroke
高脂血症	hyprlipidemia
脾胃不和	
网球肘	tennis elbow
乳腺囊肿	breast cyst
神经性厌食	anorexia nervosa
萎缩纹	striae atrophicae
流行性斑疹伤寒	epidemic typhus
原发性骨髓纤维化	primary myelofibrosis	PMF
卵泡发育不良	follicular maldevelopment	FM
创伤性气胸	traumatic pneumothorax
少精症	oligospermia
脊髓病变	spinal cord lesions
肱骨外上髁炎	lateral humeral epicondylitis
慢性淋巴细胞性甲状腺炎	chronic lymphocytic thyroiditis	CLT
上腔静脉综合征	superior vena cava syndrome	SVCS
支气管哮喘	bronchial asthma
先天性肺动静脉瘘	congenital pulmonary arteriovenous fistula	PAVF
乳溢症	galactorrhea
1型糖尿病	type 1 diabetes mellitus	T1DM
甲状腺炎	thyroiditis
梅核气	
露阴癖	exhibitionism
大网膜扭转	torsion of greater omentum
毛细胞白血病	hairy cell leukemia	HCL
贾第虫病	giardiasis
路易体痴呆	dementia with Lewy bodies	DLB
宫角妊娠	
椎动脉型颈椎病	
龟头炎	balanitis
扩张型心肌病	dilated cardiomyopathy	DCM
酪氨酸血症	tyrosinemia
遗传性果糖不耐受症	hereditary fructose intolerance	HFI
体癣	tinea corporis
卡尔曼综合征	Kallmann syndrome	KS
放射性口炎	radiation stomatitis
前列腺癌	prostate cancer
口腔颌面部囊肿	oral and maxillofacial cyst
眼袋	eye bags
隐孢子虫病	cryptosporidiosis
肺结核	pulmonary tuberculosis
阴虚火旺	
痒疹	prurigo
脾胃虚寒	
结核性胸膜炎	tuberculous pleuritis
黄斑病变	maculopathy
喉息肉	polyp of larynx
天疱疮	pemphigus
子宫内膜癌	endometrial carcinoma
慢性脓胸	chronic empyema
新生儿脓疱病	impetigo neonatorum
离心性环状红斑	erythema annulare centrifugum
躁狂症	Mania
选择性缄默症	selective mutism
中心性浆液性脉络膜视网膜病变	central serous chorioretinopathy	CSC
前列腺钙化	prostatic calcification
弥散性血管内凝血	disseminated intravascula rcoagulation	DIC
脾胃湿热	
超忆症	hyperthymesia
早产	premature delivery	PTD
阴道炎	vaginitis
玻璃体混浊	vitreous opacity
过敏性皮肤病	allergic dermatosis
妊娠合并急性膀胱炎	
生理性黄疸	physiologic jaundice
精子凝集症	sperm agglutination
肛管炎	anal canal inflammation
急性前列腺炎	Acute prostatitis
硬脑膜外脓肿	extradural abscess
曲霉病	aspergillosis
慢性支气管炎	chronic bronchitis
附睾结核	tuberculosis of the epididymis
十二指肠溃疡	duodenal ulcer	DU
眼底血管堵塞	
斜视	strabismus
矽肺	silicosis
甲状腺功能减退	hypothyroidism
卵巢早衰	premature ovarian failure	POF
肺囊性纤维化	pulmonary cystic fibrosis
原发性高血压	essential hypertension
猫抓病	cat-scratch disease
Kartagener综合征	kartagener syndrome	KS
色素性静脉旁视网膜脉络膜萎缩	pigmented paravenous retinochoroidal atrophy	PPRCA
纵隔气肿	mediastinal emphysema
骨筋膜室综合征	osteofascial compartment syndrome
结节病	sarcoidosis
色弱	color weakness
脊髓性肌萎缩症	spinal muscular atrophy	SMA
尘肺	pneumoconiosis
围绝经期综合征	perimenopausal syndrome
低血容量性休克	hypovolemic shock
代谢综合征	metabolic syndrome	MS
黄体囊肿	corpus luteum cyst
动脉硬化闭塞症	arteriosclerosis obliterans	ASO
外阴瘙痒	pruritus vulvae
胞轮振跳	
胎粪吸入综合征	meconium aspiration syndrome	MAS
慢性黏膜皮肤念珠菌病	chronic mucocutaneous candidiasis	CMC
婴儿严重肌阵挛性癫痫	severe myoclonic epilepsy in infancy	SMEI
蛛网膜囊肿	arachnoid cysts
脑干梗塞	brain stem infarction
心肾不交	
肉瘤	sarcoma
宫颈癌	cervical cancer
肘管综合征	cubital tunnel syndrome
全身型重症肌无力	generalized myasthenia gravis	GMG
肾发育不全	renal dysplasia
卵巢成熟畸胎瘤	mature teratoma of the ovary
急性呼吸窘迫综合征	acute respiratory distress syndrome	ARDS
上呼吸道感染	upper respiratory tract infection
磨牙症	bruxism
尖锐湿疣	condyloma acuminatum	CA
口腔白斑病	oral leukoplakia
隐翅虫皮炎	paederus dermatitis
黏多糖贮积症Ⅵ型	mucopolysaccharidosis type Ⅵ
肝硬化	liver cirrhosis
马牙	
肾小球肾炎	glomerulonephritis	GN
梅尼埃病	Meniere disease
口腔黏膜病	oral mucosal disease
崩漏	
阵发性睡眠性血红蛋白尿症	paroxysmal nocturnal hemoglobinuria
多发性肌炎	polymyositis
散光	astigmatism
肝阳上亢证	
肺隔离症	pulmonary sequestration
咖啡斑	
气胸	pneumothorax
阳虚	
急性结膜炎	acute conjunctivitis
枫糖尿症	maple syrup urine disease	MSUD
麦粒肿	hordeolum
鼻后滴漏综合征	postnasal drip syndrome
边缘型人格障碍	Borderline personality disorder
肺念珠菌病	pulmonary candidiasis
心包肿瘤	pericardial tumor
洁癖	
结膜炎	conjunctivitis
脑血栓形成	cerebral thuombosis
霰粒肿	Chalazion或Meibomian Cyst
慢性肾衰竭	chronic renal failure	CRF
慢性前列腺炎	chronic prostatitis	CP
高同型半胱氨酸血症	
肝郁气滞证	
泪道狭窄	stenosis of lacrimal passage
色素痣	naevus pigmentosus
卵巢内胚窦瘤	endodermal sinus tumor
低血糖	hypoglycemia
流行性腮腺炎	epidemic parotitis	mumps
手指屈肌腱鞘炎	
脊髓型颈椎病	cervical spondylotic myelopathy
脊索瘤	chordoma
呼吸性酸中毒	respiratory acidosis
近视眼	myopia
念珠菌性阴道炎	monilial vaginitis
原发性肝癌	primary carcinoma of the liver
贲门失弛缓症	achalasia of thecardia
精索静脉曲张	varicocele
乳腺导管扩张症	mammary duct ectasia	MDE
多汗症	hyperhidrosis
慢性肠炎	chronic enteritis
流行性乙型脑炎	epidemic encephalitis B
雷诺病	Raynaud disease
急性肠炎	chordapsus
神经性贪食症	bulimia nervosa
脂肪瘤	lipoma
产后出血	postpartum hemorrhage
血热	
甲状腺腺瘤	Thyroid adenoma
先兆临产	threatened labor
漏斗胸	pectus excavatum	PE或funnel chest
睑板腺炎	hordeolum
腱鞘炎	tenosynovitis
脑栓塞	cerebral embolism
功能性便秘	functional constipation
混合性结缔组织病	mixed connective tissue disease	MCTD
血虚	
前庭大腺炎	bartholinitis
肺性脑病	pulmonary encephalopath
乳腺纤维腺瘤	fibroadenoma
色盲	color blindncss
过敏性哮喘	allergic asthma
脂溢性角化病	seborrheic keratosis
龋病	dental caries
视网膜脱离	retinal detachment	RD
卵巢肿瘤	ovarian tumor
无精子症	azoospermia
过敏性休克	anaphylactic shock
脊髓小脑性共济失调	spinocerebellar ataxia	SCA
周期性麻痹	periodc paralysis
小儿荨麻疹	pediatric urticaria
链球菌感染	
产褥感染	puerperal infection
湿疣	
胸痹心痛	
主动脉瓣狭窄	aortic stenosis	AS
华支睾吸虫病	clonorchiasis sinensis
氟牙症	dental fluorosis
牙隐裂	cracked tooth
骨软骨瘤	osteochondioma
色素性荨麻疹	urticaria pigmentosa
线粒体病	mitochondrial disease
心肌梗死	myocardial infarction	MI
中耳炎	otitis media
肺脓肿	lung abscess
脾功能亢进	hypersplenism
黑热病	Kala-azar
动脉瘤样骨囊肿	aneurysmal bone cyst	ABC
脊柱结核	tuberculosis of spine
纠正性大动脉错位	corrected transposition of great arteries	CTGA
高胆红素血症	
肛乳头肥大	
急性盆腔炎	acute pelvic inflammatory disease
肥胖症	obesity
自然流产	spontaneous abortion
法洛四联症	tetralogy of Fallot	Fallot tetrad	TOF
抗磷脂抗体综合征	antiphospholipid antibody syndrome
压疮	decubitus
原发性肉碱缺乏症	primary carnitine deficiency	PCD
森林脑炎	forest encephalitis
维生素D缺乏症	
胶质母细胞瘤	glioblastoma
多系统萎缩	Multiple system atrophy	MSA
急性牙髓炎	acute pulpitis
异食癖	pica
门静脉高压	portal hypertension
急性咽喉炎	acute laryngopharyngitis
睡眠呼吸暂停综合征	sleep apnea syndrome	SAS
炎性肉芽肿	inflammatory granuloma
心脏病	heart disease
心源性休克	cardiogenic shock
二尖瓣狭窄	mitral stenosis	MS
疝气	hernia
单纯性甲状腺肿	simple goiter
高钙血症	hypercalcemia
瘰疬	老鼠疮
汗疱症	pompholyx
肾髓质囊性病	
急腹症	acute abdomen
泄泻	
根尖周病	periapical diseases
减压病	decompression sickness	DCS
臭汗症	bromhidrosis
茎突综合征	
暴食症	binge eating disorder
肺胀	lung distention
非火器性颅脑开放伤	non-firearm open craniocerebral injury
阴道腺病	vaginal adenosis
一度房室传导阻滞	Ⅰ° atrioventricular block	Ⅰ° AVB
拇囊炎	bunions
肾结核	renal tuberculosis
高催乳素血症	hyperprolactinemia
肌筋膜炎	
扁桃体炎	tonsillitis
月经过多	heavy menstrual bleeding	HMB
烧伤	burns
乳突炎	mastoiditis
胃寒	
脚气病	
褥疮	decubitus
慢性肾脏病	chronic kidney disease	CKD
双子宫	didelphic uterus
足内翻	
单纯性肾囊肿	simple renal cysts	SRC
右室心肌梗死	right ventricular myocardial infarction	RVMI
颅内畸胎瘤	intracranial teratoma
骨血管瘤	hemangioma of bone
颞骨骨折	temporal bone fracture
红细胞增多症	erythrocytosis
急性青光眼	acute glaucoma
女性尖锐湿疣	condyloma acuminatum in women
流行性感冒	influenza
垂体瘤	pituitary tumor
红眼病	pink eye
黄斑前膜	epiretinal membrane
躯体形式障碍	somatoform disorders
肺热	
气滞血瘀证	
奔豚气	
牙龈瘤	epulis
阿米巴肠病	
纵隔肿瘤	mediastinal tumors
跟腱断裂	
儿童癫痫	Children epilepsy
免疫缺陷病	immuno deficiency disorders
心包炎	pericarditis
阑尾炎	appendicitis
牙结石	dental calculus
窦性心动过速	sinus tachycardia
空鼻综合征	empty nose syndrom
新生儿生理性黄疸	neonatal physiological jaundice
脑囊虫病	cerebral cysticercosis
鼻出血	epistaxis
羊水过多	polyhydramnios
内眦赘皮	epicanthus
远视	hypermetropia或hyperopia
脐带绕颈	
肺气肿	emphysema
睾丸结核	tuberculosis of testis
脊髓前动脉综合征	anterior spinal artery syndrome	ASAS
隐匿阴茎	concealed penis	inconspicuous penis
脑脊液鼻漏	cerebrospinal rhinorrhea
主动脉瓣关闭不全	aortic insufficiency	AI
痴呆	dementia
发作性睡病	narcolepsy
原发性痛经	primary dysmenorrhoea
广泛性焦虑障碍	generalized anxiety disorder
库欣综合征	cushing syndrome	CS
神经根型颈椎病	cervical spondylotic radiculopathy	CSR
淋巴水肿	lymphedema
肌营养不良症	muscular dystrophy
白细胞减少症	leukopenia
乳痈	
慢性盆腔炎	chronic pelvic inflammatory disease
姜片虫病	fasciolopsiasis
急性脊髓炎	acute myelitis
气性坏疽	gas gangrene
喉结核	tuberculosis of the larynx
疲劳综合征	
老花眼	Presbyopia
脑胶质瘤	
潜伏梅毒	latent syphilis
霉菌性食管炎	fungal esophagitis
血清病	serum sickness
颞叶肿瘤	temporal lobe tumor
胆汁反流性胃炎	bile reflux gastritis	BRG
小儿腹泻	pediatric diarrhea
腘窝囊肿	popliteal cyst
先天性纯红细胞再生障碍性贫血	Aplasia Congenital Pure Red Cell
不孕症	infertility
男性乳房发育症	gynecomastia	GYN
副银屑病	parapsoriasis
泪道阻塞	stenosis of lacrimal passage
泪囊炎	dacryocystitis
乳腺钙化	
睾丸肿瘤	tumor of testis
冻伤	cold injury
腮腺癌	carcinoma of the parotid gland
虚劳	consumptive disease
选择困难症	
阴道息肉	vaginal polyps
快慢综合征	
肝著	liver fixity
卵巢纤维瘤	ovarian fibroma
阻塞性肺气肿	
巩膜炎	scleritis
上睑下垂	ptosis
视网膜色素变性	retinitis pigmentosa	RP
视盘水肿	optic disc edema
短肠综合征	short bowel syndrome
高钠血症	hypernatremia
风湿性心脏病	rheumatic heart disease	RHD
风湿性多肌痛	polymyalgia rheumatic	PMR
重症先天性粒细胞缺乏症	severe congenital neutropenia	SCN
螨虫皮炎	mite dermatitis
癃闭	
肾精亏虚	
巨细胞病毒感染症	
红皮病	erythroderma
黄斑裂孔	macular hole
球孢子菌病	coccidioidomycosis
新疆出血热	Xinjiang hemorrhagic fever	XHF
五更泄泻	
肛门失禁	anal incontinence
老年高血压	senile hypertension
乳腺炎	mastitis
多毛症	hypertrichosis
屈光不正	refractive error
淋巴结炎	lymphadenitis
肠瘘	intestinal fistula
烟酸缺乏症	aniacinosis
盆腔淤血综合征	pelvic congestion syndrome
低温症	hypothermia
小儿佝偻病	neonatalrickets
钩端螺旋体病	leptospirosis
胰岛素瘤	insulinoma
膈疝	diaphragmatic hemia
闭锁综合征	locked-in syndrome	LIS
适应障碍	adjustment disorder
小儿缺铁性贫血	
三尖瓣闭锁	tricuspid atresia	TA
肝炎	hepatitis
夜惊症	sleep terror
脑损伤	brain damage
鞘膜积液	hydrocele
先天性肌强直	myotonia congenita
大动脉炎	takayasu arteritis	TAK
口吃	stuttering
肝阴虚	
坐板疮	
睑裂斑	pinguecula
回归热	relapsing fever
动脉栓塞	arterial embolism
胃泌素瘤	
十二指肠息肉	
新生儿缺氧缺血性脑病	hypoxic-ischemic encephalopathy	HIE
多种酰基辅酶A脱氢酶缺乏症	multiple acyl-CoA dehydrogenase deficiency	MADD
孢子丝菌病	sporotrichosis
支原体尿路感染	mycoplasma urinary tract infection
颜面部疖痈	
外阴纤维瘤	fibroma of vulva
腹膜假黏液瘤	pseudomyxoma peritonei	PMP
三尖瓣关闭不全	tricuspid regurgitation	TR
牙龈炎	gingivitis
Castleman病	Castleman disease	CD
纯合子家族性高胆固醇血症	homozygous familial hypercholesterolemia	HoFH
Gitelman综合征	Gitelman syndrome	GS
肱二头肌长头腱鞘炎	
遗传性低镁血症	Inherited Hypomagnesemia
脑干损伤	brain stem injury
肾性骨营养不良	renal osteodystrophy	ROD
胎儿生长受限	fetal growth restriction	FGR
手腕腱鞘囊肿	
肝火犯肺	
中毒	poisoning
急性乳腺炎	acute mastitis
失血性休克	hemorrhagic shock
鼻前庭炎	nasal vestibulitis
食物过敏	food allergy
新生儿窒息	asphyxia of newborn
副伤寒	paratyphoid fever
黄热病	yellow fever
胃扭转	gastric volvulus
肱骨髁上骨折	supracondylar fracture of humerus
噬血细胞综合征	hemophagocytic syndrome	HPS
输卵管癌	Carcinoma of Fallopian Tube
内伤发热	endogenous fever
难治性癫痫	refractory epilepsy、intractable epilepsy	IE
内痔	internal hemorrhoid
色素沉着绒毛结节性滑膜炎	pigmented villonodular synovitis	PVNS
急性心包炎	acute pericarditis
房性期前收缩	premature atrial beats	premature atrial contraction	 PAC
睑缘炎	blepharitis
涎腺囊肿	
火激红斑	erythema ab igne
病毒性心肌炎	viral myocarditis	VMC
特发性肺纤维化	idiopathic pulmonary fibrosis	IPF
肺栓塞	pulmonary embolism	PE
垂体腺瘤	pituitary adenoma	PA
化脓性中耳炎	suppurative otitis media
产后便秘	
婴幼儿腹泻	infantile diarrhea
胃气上逆	
小儿疳积	
甲状舌管囊肿	thyroglossal duct cyst
病毒性肺炎	viral pneumonia
肺孢子菌肺炎	pneumocystis pneumonia	PCP
药物性鼻炎	rhinitis medicamentosa
射精功能障碍	ejaculatory dysfunction
流感嗜血杆菌感染	Hemophilus influenzae
蹄铁形肾	horseshoe kidney
室管膜肿瘤	
舌病	tongue diseases
瑞氏综合征	Reye Syndrome	RS
硬皮病	scleroderma
颈椎后纵韧带骨化症	ossification of posterior longitudinal ligament of the cervical spine	C-OPLL
血管性血友病	von Willebrand disease	vWD
单纯肾异位	simple renal ectopia
尤因肉瘤	Ewing sarcom
灼口综合征	burning mouth syndrome	BMS
主动脉夹层	aortic dissection	AD
眼睑脓肿	abscess of the eyelids
小儿癫痫	pediatric epilepsy
智力障碍	disturbance of intelligence
溃疡性直肠炎	ulcerative rectitis
周围性面瘫	peripheral facial paralysis
子痫	eclampsia
眼睑下垂	ptosis
结节性硬化症	tuberous sclerosis	TS
牙龈萎缩	gingival recession
慢性肾炎	chronic nephritis
胫腓骨骨折	
鲍温样丘疹病	Bowenoid papulosis
高眼压症	ocular hypertension
新生儿硬肿症	scleredema neonatorum 
儿童遗尿症	Enuresis in children
子宫发育异常	Uterine dysplasia
喉痹	
小脑扁桃体下疝畸形	Arnold-Chiari malformation
胆道蛔虫病	biliary ascariasis
葡萄球菌感染	staphylococci infection
囊性纤维化	cystic fibrosis	CF
鼻骨骨折	fracture of nasal bone
肱骨外科颈骨折	humeral surgical neck fracture
健忘症	amnesia
脊髓休克	spinal shock
血管肉瘤	angiosarcoma
肺挫伤	pulmonary contusion
睾丸破裂	testicular rupture
汗腺炎	hidrosadenitis
真两性畸形	true hermaphroditism
腭裂	cleft palate
肾上腺皮质功能减退症	adrenal insufficiency	AI
阴道斜隔	oblique vaginal septum
外阴银屑病	psoriasis of vulva
妊娠期急性脂肪肝	acute fatty liver of pregnancy	AFLP
面肌瘫痪	
癫痫持续状态	status epilepticus	SE
应激性溃疡	stress ulcer	SU
朗格汉斯组织细胞增多症	Langerhans’cell histiocytosis	LCH
骨样骨瘤	osteoid osteoma
难产	dystocia
心阴虚	
新生儿泪囊炎	neonatal dacryocystitis
钩虫病	ancylostomiasis	hookworm disease
弓形虫病	toxoplasmosis
泪腺炎	dacryoadenitis
生长激素缺乏症	growth hormone deficiency	GHD
奶癣	
眼睛近视	myopia
阴茎扭转	distortion of penis
肾损伤	renal injuries
中东呼吸综合征	Middle East respiratory syndrome	MERS
桡神经损伤	radial nerve injury
葡萄球菌肺炎	staphylococcal pneumonia
地方性克汀病	endemic cretinism
额叶癫痫	frontal lobel epilepsy
脓性指头炎	felon
主动脉夹层破裂	aortic dissection rupture
嗜酸性粒细胞增多症	eosinophilia
前哨痔	sentinel pile
贲门息肉	cardiac polyp
肾性尿崩症	nephrogenic diabetes insipidus	NDI
侵蚀性葡萄胎	invasive mole
软骨肉瘤	chondrosarcoma
髋关节骨关节炎	coxarthrosis
慢性肥厚性鼻炎	chronic hypertrophic rhinitis
毛母质瘤	pilomatricoma	PM
下消化道出血	lower gastrointestinal hemorrhage
心房颤动	atrial fibrillation
先天性肾病综合征	congenital nephrotic syndrome 	CNS
子宫破裂	uterine rupture
羊水栓塞	amniotic fluid embolism	AFE
视神经萎缩	optic atrophy
倒睫	trichiasis
原发性轻链型淀粉样变	Primary Light Chain Amyloidosis	pAL
女性不孕症	famale infertility
间质性肺疾病	interstitial lung diseases	ILDs
小肠吸收不良综合征	intestinal malabsorption syndrome
脑脓肿	brain abscess
化脓性肉芽肿	pyogenic telangiectaticum
咽异感症	abnormal sensation of throat
低血糖症	hypoglycemia
拉沙热	Lassa fever
肺泡蛋白沉积症	pulmonary alveolar proteinosis	PAP
原发性甲状旁腺功能亢进症	primary hyperparathyroidism	PHPT
产后恶露不尽	
乳腺结核	breast tuberculosis
阴道闭锁	atresia of vagina
颅脑损伤	craniocerebral injury
儿童过敏性咳嗽	
软组织肿瘤	Soft tissue tumor
气管炎	tracheitis
皮肤炭疽	cutaneous anthrax
热衰竭	heat exhaustion
泌乳素瘤	prolactinoma
先天性小眼球合并眼眶囊肿	congenital microphthalmos with orbital cyst
房室折返性心动过速	atrioventricular reentrant tachycardia	AVRT
盘尾丝虫病	onchocerciasis
妊娠合并肺结核	pregnancy with tuberculosis
膀胱结石	vesical calculi
先天性垂直距骨	congenital vertical talus	CVT
结膜结石	conjunctival concretion
病毒性腹泻	
智能障碍	disturbance of intelligence
亚急性甲状腺炎	subacute thyroiditis	SAT
脐疝	umbilical hernia
急性肾小球肾炎	acute glomerulonephritis
先天性脊柱侧凸	congenital scoliosis	CS
头皮血肿	scalp hematoma
镰刀型细胞贫血病	sickle cell disease
先天性脑积水	congenital hydrocephalus	CH
新生儿巨细胞病毒感染	cytomegalovirus infection
非典型溶血性尿毒症综合征	atypical hemolytic uremic syndrome
高苯丙氨酸血症	hyperphenylalaninemia	HPA
POEMS综合征	POEMS Syndrome
先天性肾上腺皮质增生症	congenital adrenal cortical  hyperplasia	CAH
伪膜性肠炎	pseudomembranous colitis	PMC
死精症	dead spermatozoon sickness
囊虫病	cysticercosis
麻痹性斜视	paralytic strabismus
颌骨骨髓炎	osteomyelitis of jaw
家族性地中海热	familial Mediterranean fever	FMF
早期乳腺癌	early mammary cancer	EBC
位置性眩晕	positional vertigo
瘢痕妊娠	
结肠损伤	injury of colon
小舞蹈病	chorea minor
先天性肌无力综合征	congenital myasthenic syndrome	CMS
马尔堡病毒病	
外伤性癫痫	post traumatic epilepsy 	PTE
异位胰腺	heterotopic pancreas
小头畸形	microcephaly
脊髓小脑变性症	spinocerebellar ataxia	SCA
韦格纳肉芽肿	Wegener's granulomatosis 	WG
大疱性表皮松解症	epidermolysis bullosa	EB
异戊酸血症	isovaleric academia	IVA
环状肉芽肿	granuloma annulare	GA
骨关节病	osteoarthrosis
精索恶性肿瘤	malignant tumor of spermatic cord
假性动脉瘤	pseudoaneurysm	PSA
进行性家族性肝内胆汁淤积症	progressive familial intrahepatic cholestasis	PFIC
原发性肺动脉高压	primary pulmonary hypertension	PPH
鼓胀	tympanites
内翻性乳头状瘤	inverted  papilloma
颅狭症	craniostenosis
咳嗽晕厥综合征	cough syncope syndrome
史密斯骨折	smith's fracture
海绵窦血栓	cavernous sinus thrombosis	CST
喉癌	laryngeal carcinoma
阴道黑色素瘤	vaginal melanoma
先天性膈疝	congenital diaphragmatic hernia	CDH
卵巢无性细胞瘤	dysgerminoma
小儿脑积水	
眼底水肿	
老年抑郁症	depression in the elderly
虹膜睫状体炎	iridocyclitis
支气管扩张症	bronchiectasis
法布里病	fabry disease
肉芽肿性唇炎	granulomatosa cheilitis
学习障碍	
肺吸虫病	lung fluke disease
脑型疟疾	cerebral malaria
盆腔脓肿	pelvic abscess
网状青斑	livedo reticularis	LR
放射性肺炎	Radiation pneumopathy
急性化脓性甲状腺炎	acute suppurative thyroiditis	AST
牙折	tooth fracture
Leber遗传性视神经病变	Leber hereditary optic neuropathy	LHON
造影剂肾病	contrast-induced nephrology	CIN
进食障碍	eating disorders
先天性无阴道	congenital absence of vagina
肩锁关节脱位	dislocation of the acromioclavicular joint
真性近视	true myopia
心脏离子通道病	
输尿管肿瘤	ureteral tumor
嘌呤代谢障碍	
膝关节侧副韧带损伤	
颞叶癫痫	temporal lobe epilepsy	TLE
急性气管支气管炎	acute tracheobronchitis
肠系膜上动脉栓塞	superior mesenteric arterial embolism	superior mesenteric artery embolus
淋巴结结节病	lymph node sarcoidosis
蕈样肉芽肿	Granuloma Fungoides
摩擦性苔藓样疹	frictional lichenoid eruption
血胸	hemothorax
肺动静脉瘘	Arteriovenous fistula of the lung
女性生殖器疱疹	female genital herpes
先天性巨痣	congenital giant nevus
皮角	cutaneous horn
非特异性尿道炎	nonspecific urethritis
毒蛇咬伤	venomous snake bite
坏疽性脓皮病	pyoderma gangrenosum 	PG
化脓性关节炎	suppurative arthritis	SA
早产儿视网膜病变	retinopathy of prematurity	ROP
气管肿瘤	Tracheal Neoplasms
二氧化氮中毒	nitrogen dioxide poisoning
二氧化硫中毒	sulfur dioxide poisoning
肝上皮样血管内皮细胞瘤	hepatic epithelioid hemangioendothehoma
喉上神经损伤	superior laryngeal nerve injury
节段性透明性血管炎	segmental hyalinizing vasculitis
肾嗜酸细胞瘤	renal oncocytoma	 RO
乳管内乳头状瘤	intraductal papilloma
急性腐蚀性胃炎	acute corrosive gastritis
慢性盆腔痛	chronic pelvic pain	CPP
诺卡菌病	nocardiosis
恐怖性焦虑障碍	
妊娠合并急性胆囊炎	acute cholecystitis in pregnancy
膈肌麻痹	diaphragmatic paralysis
外阴鳞状上皮细胞增生	squamous cell hyperplasia of vulua
心包囊肿	pericardial cyst
闭合性脊髓损伤	closed spinal cord injury
加利福尼亚脑炎	california encephalitis	CE
唇炎	cheilitis
混合型青光眼	mixed glaucoma
赖氨酸尿蛋白不耐受症	lysinuric protein intolerance	LPI
病毒性角膜炎	viral keratitis
丙酸血症	propionic acidemia	PA
头风病	
唇癌	carcinoma of the lip
牙瘤	odontoma
膝韧带损伤	knee ligament injury
光泽苔藓	lichen nitidus
自身免疫性垂体炎	autoimmune hypophysitis
范科尼贫血	Fanconi anemia
卵巢过度刺激综合征	ovarian hyperstimulation syndrome
急性播散性脑脊髓炎	acute disseminated encephalomyelitis	ADEM
遗传性痉挛性截瘫	hereditary spastic paraplegia	HSP
异装症	transvestitism
戊二酸血症Ⅰ型	glutaric acidemia Ⅰ	GA-1
移植物抗宿主病	graft versus host disease	GVHD
解脲支原体感染	Ureaplasma urealyticum infection
利什曼病	leishmaniasis
失荣	cervical carcinoma with cachexia
远端肾小管性酸中毒	distal renal tubular acidosis	dRTA
骨不连	bone nonunion
宫寒不孕	
胎漏	threatened abortion
肘关节脱位	dislocation of elbow joint
弹响髋	snapping hip	SH
菜花病	condyloma acuminatum	CA
膀胱外翻	bladder exstrophy
热痉挛	heat cramp
性别认同障碍	gender identity disorder
小儿急性荨麻疹	pediatric acute urticaria
慢性细菌性前列腺炎	Chronic bacterial prostatitis 
皮肤松弛症	cutis laxa	CL
神经管畸形	neural tube defects	NTDs
软骨母细胞瘤	chondroblastoma	CB
呼吸道合胞病毒感染	respiratory syncytial virus infection
肾静脉血栓	renal vein thrombosis	RVT
结核性溃疡	Tuberculous ulcer
Chance骨折	chance fracture
肱骨大结节骨折	fracture of greater tuberosity of humerus
阑尾肿瘤	appendixcancer
腰疝	lumbarhernia
卵巢透明细胞癌	ovarian clear cell adenocarcinoma	OCCA
门静脉高压性胃病	portal hypertensive gastropathy 	 PHG
蛲虫性阴道炎	threadworm vaginitis
吻合口溃疡	stomal ulcer
阴道裂伤	vaginal laceration
结肠脂肪瘤	
足菌肿	mycetoma
类肺炎性胸腔积液	parapneumonic effusions
血吸虫肠病	
等孢球虫病	isosporiasis
急性间歇性卟啉病	acute intermittent porphyria	AIP
泪囊堵塞	
翼状胬肉	pterygium
吸入性肺炎	aspiration pneumonitis
后天斜视	acquired strabismus 
遗传性大疱性表皮松解症	hereditary epidermolysis bullosa
遗传性多发脑梗死性痴呆	genetic multi-infarct dementia
N-乙酰谷氨酸合成酶缺乏症	N-acetylglutamate synthase deficiency	NAGSD
嘌呤代谢异常	
正中神经损伤	median nerve injury
老年性黄斑变性	senile macular degeneration
慢性泪囊炎	chronic dacryocystitis
唇外翻	chilectropion
特发性关节痛综合征	特发性关节痛综合征
新生儿腹泻	neonatal diarrhea
急性出血性坏死性小肠炎	acute hemorrhagic necrotizing enteritis	AHNE
恶性高热	malignant hyperthermia	MH
剖宫产瘢痕妊娠	cesarean scar pregnancy	CSP
胃肠道菌群失调	gastrointestinal flora disorders
无痛性甲状腺炎	painless thyroiditis	PT
溶酶体酸性脂肪酶缺乏症	lysosomal acid lipase deficiency	LALD
气管支气管异物	tracheobronchial foreign body
白线疝	herniaof linea alba
膈下脓肿	subphrenic abscess
吞气症	aerophagia
麦胶性肠病	gluten-induced enteropathy
胰高血糖素瘤	glucagonoma	GCGN
类鼻疽	melioidosis
肾淀粉样变性	renal amyloidosis
小儿急性阑尾炎	pediatric acute appendicitis
颌骨囊肿	jaw cyst
支气管肺炎	bronchopneumonia
芽生菌病	Blastomycosis
传染性湿疹样皮炎	dermatitis eczematoides infectiosa
性病性淋巴肉芽肿	lymphogranulomavenereum	LGV
疣状表皮发育不良	epidermodysplasia verruciformis	EV
脑性瘫痪综合征	cerebral palsy syndrome
妊娠痒疹	prurigo gestationis
小儿急性胰腺炎	acute pancreatitis in children
脐风	
部分性葡萄胎	partial hydatidiform mole
鼠咬热	rat bite fever	RBF
赤面恐惧症	blushing and exythmphobia
尿道黏膜脱垂	urethral mucosa prolapse
外阴血管瘤	vulvar hemangioma
韧带炎	
网络依赖症	
肾动脉栓塞	renal artery embolism
腹膜间皮瘤	peritoneal mesothelioma	PM
单纯疱疹性脑炎	herpes simplex virus encephalitis	 HSE
动静脉血管瘤	arteriovenous hemangioma
青年上肢远端肌萎缩	juvenile muscular atrophy of distal upper extremity
颅骨缺损	defect of skull
乳腺叶状囊肉瘤	cystosarcoma phyllodes	 CP
群体性癔症	
特发性股骨头坏死	
三叉神经营养性损害	trigeminal trophic lesions
骨纤维肉瘤	fibrosarcoma of bone
嗅沟脑膜瘤	olfactory groove meningioma
淤积性皮炎	stasis dermatitis
原发性硬化性胆管炎	primary sclerosing cholangitis	PSC
真菌性食管炎	fungal esophagitis
脉络膜黑色素瘤	melanoma of choroid
原发性输卵管癌	primary carcinoma of the fallopian tube
氨基酸代谢病	aminoacidopathy
药物性牙龈增生	drug-induced gingival enlargements
阿米巴性阴道炎	ameba vaginitis
妊娠合并急性肾盂肾炎	
妊娠合并肺栓塞	pregnancy with pulmonary embolism
小儿喉痉挛	infantile laryngeal spasm
荚膜组织胞浆菌病	histoplasmosis capsulati
异尖线虫病	anisakiasis
假膜性肠炎	pseudomembranous colitis	PMC
出血性输卵管炎	hemorrhagic salpingitis
子宫恶性中胚叶混合瘤	malignant mixed mesodermal tumor	MMMT
卵巢未成熟畸胎瘤	ovarian immature teratoma
房室管畸形	
急性嗜酸性粒细胞性肺炎	acute eosinophilic pneumonia	AEP
慢性阻塞性腮腺炎	chronic obstructive parotitis
无性细胞瘤	dysgeminoma
丘脑下部损伤	hypothalamic injury
心脏横纹肌瘤	cardiac rhabdomyoma	CR
职业性哮喘	occupational asthma
脊髓火器伤	
先天性风疹综合征	congenital rubella syndrome、CRS
颅骨结核	tuberculosis of skull
脉络丛乳头状瘤	choroid plexus papilloma
阴道异物	vaginal foreign body
颅后窝血肿	hematoma of posterior fossa
外阴硬化性苔藓	vulvar lichen sclerosus	VLS
外伤后低颅压综合征	posttraumatic intracranial hypotension syndrome
椎管内脊膜瘤	intraspinal meningioma
女阴白斑	leukoplakia vulvae
流感病毒肺炎	influenza virus pneumonia
急性泪囊炎	acute dacryocystitis
外阴炎	vulvitis
胎膜早破	premature rupture of membranes	PROM
胎儿窘迫	fetal distress
点状白内障	punctate cataract
眼底静脉出血	fundus vein bleeding
乳腺囊性增生病	breast cystic hyperplasia
全羧化酶合成酶缺乏症	holocarboxylase synthetase deficiency	HCSD
先天性肾上腺发育不良	congenital adrenal hypoplasia	adrenal hypoplasia congenita
先天性高胰岛素性低血糖血症	congenital hyperinsulinemic hypoglycemia	CHI
X连锁肾上腺脑白质营养不良	X-linked adrenoleukodystrophy	X-ALD
原发性遗传性肌张力不全	primary hereditary dystonia	PHD
X连锁淋巴增生症	X-linked lymphoproliferative disease	XLP
生物素酶缺乏症	biotinidase deficiency	BTDD
谷固醇血症	sitosterolemia
先天性胆汁酸合成障碍	inborn errors of bile acid synthesis	IEBAS
自身免疫性胰岛素受体病	autoimmune insulin receptopathy
莱伦综合征	Laron syndrome
β-酮硫解酶缺乏症	β-ketothiolase deficiency	BKD
精氨酸酶缺乏症	arginase deficiency
涎石病	sialolithiasis
遗传性血管性水肿	hereditary angioedema
肱骨内上髁骨折	fracture of medial epicondyle of humerus
特发性心肌病	
喉喑	
血证	
老年尿失禁	Urinary incontinence in the elderly
放线菌病	actinomycosis
多灶性运动神经病	multifocal motor neuropathy	MMN
极长链酰基辅酶A脱氢酶缺乏症	very long chain acyl-CoA dehydrogenase deficiency	VLCADD
兔热病	rabbit fever	tularemia
慢性泪腺炎	chronic dacryoadenitis
盲袢综合征	blind loop syndrome
包涵体肌炎	inclusion body myositis	IBM
Erdheim-Chester病	Erdheim-Chester Disease 	 ECD
鸟氨酸氨甲酰基转移酶缺乏症	ornithine transcarbamylase deficiency	OTCD
特发性肺动脉高压	
中链酰基辅酶A脱氢酶缺乏症	medium chain acyl-CoA dehydrogenase deficiency	MCADD
炎性乳癌	
小儿功能性消化不良	functional dyspepsia	FD
骨与关节结核	tuberculosis of bone and joint	bone and joint tuberculosis
糖尿病心肌病	diabetic cardiomyopathy	DCM
近端肾小管性酸中毒	proximal renal tubular acidosis	pRTA
皮肤蝇蛆病	myiasis cutis
黏多糖贮积症Ⅶ型	mucopolysaccharidosis type Ⅶ
热烧伤	thermal injury
自主性高功能性甲状腺腺瘤	autonomous hyperfunctioning adenoma
哮喘性支气管炎	asthmatic bronchitis
妊娠合并梅毒	syphilis in pregnancy
色素失禁症	Incontinentia pigmenti	IP
瓜氨酸血症	citrullinemia
慢性食管炎	chronic esophagitis
妊娠合并弓形虫病	toxoplasmosis in pregnancy
阴茎结核	tuberculosis of the penis
妊娠疱疹	herpes gestationis
毛发上皮瘤	trichoepithelioma
胎热	neonatal fever
肾皮质化脓性感染	renocortical pyogenic infection
纤维化综合征	fibrosis syndrome
胎萎不长	retarded growth of fetus
男性生殖系结核	male genital tuberculosis
结节性血管炎	nodular vasculitis
男性性功能障碍	male sexual dysfunction
脊椎结核	spine tuberculosis	
厌氧菌肺炎	anaerobic bacteria pneumonia
急性泪腺炎	acute dacryoadenitis
肝血管平滑肌脂肪瘤	hepatic angiomyolipoma	HAML
小儿食管异物	pediatric esophageal foreign body
重症联合免疫缺陷病	severe combined immunodeficiency disease	SCID
鼻内翻性乳头状瘤	nasal inverted papilloma
精囊结石	calculus of seminal vesicle
纵隔囊肿及肿瘤	mediastinal cysts and tumors
宾斯旺格病	Binswanger disease
造口旁疝	parastomalhernia
耻骨炎	osteitis pubis
药物性心肌病	drug-induced cardiomyopathy
红斑性肢痛症	erythromelalgia	EM
脉络膜脱离型视网膜脱离	retinal detachment and choroidal detachment
宫颈残端癌	stump carcinoma of the cervix
惠普尔病	Whipple’s disease	WD
后十字韧带损伤	posterior cruciate ligament	PCL
急性附件炎	acute adnexitis
颈动脉窦综合征	carotid sinus syndrome	 CCS
电脑病	
乳腺Paget‘s病	Paget's disease
暴发性紫癜	purpura fulminans	PF
微小病变型肾病	minimal change disease	MCD
食管囊肿	esophageal cyst
小脑幕脑膜瘤	tentorial meningioma
皮肤白细胞破碎性血管炎	cutaneous leukocytoclastic angitis/vasculiti
胸主动脉夹层动脉瘤	thoracic aortic dissection
青少年及小儿阴道透明细胞癌	
隐匿型乳腺癌	occult breast cancer	OBC
乳腺癌皮肤转移	cutaneous metastases from breast cancer
阴道恶性肿瘤	malignant vaginal tumor
重链病	heavy chain diseases	HCD
眼眶真菌病	orbital mycosis
眼眶淋巴管瘤	orbital lymphangioma
脑棘球蚴病	cerebral echinococcosis
咽部异物	
颚口线虫病	gnathostomiasis
童样痴呆	puerilism
